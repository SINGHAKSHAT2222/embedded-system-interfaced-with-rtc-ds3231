# Embedded System interfaced with RTC-DS3231

Interfacing Embedded Systems to the Real World: you are required to develop a self-contained, embedded system that interfaces to the real world using electronic sensors. High-level software must be written to wrap the low-level interface.